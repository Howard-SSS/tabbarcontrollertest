//
//  SceneDelegate.swift
//  NyTest11
//
//  Created by Howard on 2021/7/15.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    static let tabBar = UITabBarController()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
        SceneDelegate.tabBar.tabBar.backgroundColor = UIColor.clear
        
        let view0 = ViewController()
        view0.tabBarItem = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 0)
        let view1 = ViewController1()
        view1.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 1)
        let view2 = ViewController2()
        view2.tabBarItem = UITabBarItem(tabBarSystemItem: .downloads, tag: 2)
        
        SceneDelegate.tabBar.viewControllers = [view0,view1,view2]
        
        SceneDelegate.tabBar.selectedIndex = 0
        
        window?.backgroundColor = .darkGray
        
        window?.rootViewController = SceneDelegate.tabBar
        
        window?.makeKeyAndVisible()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

