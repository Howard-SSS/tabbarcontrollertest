//
//  ViewController2.swift
//  NyTest11
//
//  Created by Howard on 2021/7/15.
//

import UIKit

class ViewController2: UIViewController {

    let fullSize = UIScreen.main.bounds.size
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 280, height: 60))
        button.center = CGPoint(x: fullSize.width*0.5, y: fullSize.height*0.5)
        button.setTitle("jump next", for: .normal)
        button.addTarget(self, action: #selector(jump), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    @objc func jump() {
        let index = SceneDelegate.tabBar.selectedIndex
        if let num = SceneDelegate.tabBar.viewControllers?.count {
            if index + 1 == num {
                SceneDelegate.tabBar.selectedIndex = 0
            } else {
                SceneDelegate.tabBar.selectedIndex = index + 1
            }
        }
    }
}
